# GameBoy Jam Project - Team 07

Project made for [GameBoy Jam](https://itch.io/jam/cea-rpd-gd03gt01-gameboy) using following constraints:

    Input Constraint  - B, A, Start, Select, D-pad

    Design Constraint - 2D

    Visual Constraint - 160x144 pixels; Monochrome 2-bit (one color/4shades);  Only sprites - no polygons.

    Technical Constraint - Default size + 32 mb

---

## Overview

### Description

    RPD Adventure is a third person turn based mini-RPG game.  
    The player will control a CEA student and fight all the assignment monsters on the campus.  
    The player can find useful items while exploring the map, and the items will come in handy when encountering battle.  

### Goals

    The design goal of this game is to gain experience on making Game Boy styled 2D game,  
    simulate student life and getting familiar with RPG/turn based combat design.

### Game Pillars

    Three pillars that will help inform your design decisions.   
    Explore and collect.  
    Turn based combat.  
    Simulation.  

---

## References
Unity's [RPG creator kit](https://learn.unity.com/project/creator-kit-rpg)

Unity's [Dragon Crashers 2D](https://assetstore.unity.com/packages/essentials/tutorial-projects/dragon-crashers-2d-sample-project-190721)

---
