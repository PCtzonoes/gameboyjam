using System;
using UnityEngine;

[CreateAssetMenu(menuName = "GameData/Player")]
public class PlayerData : ScriptableObject
{
    public GameEvent OnDrink;
    public static readonly String[] Actions =
    {
        "Do Homework",
        "Pull All-nighter",
        "Drink Coffee"
    };

    public float Life;
    public float Damage;
    public int CoffeAmount;
    public float CoffeHeal;

    public void Reset()
    {
        Life = 100;
        Damage = 25;
        CoffeAmount = 3;
        CoffeHeal = 50;
    }

    public void DrinkCoffe()
    {
        if (CoffeAmount > 1 && Life <= 75)
        {
            OnDrink.Invoke();
            Life += CoffeHeal;
            CoffeAmount--;
        }
    }
}

