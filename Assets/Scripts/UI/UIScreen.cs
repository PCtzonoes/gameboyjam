﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class UIScreen : MonoBehaviour
{
    [SerializeField] protected UnityEngine.UI.Button _firstButton;

    protected virtual void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(_firstButton.gameObject);
        // _firstButton.Select();

    }
}
