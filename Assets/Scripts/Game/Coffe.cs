using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coffe : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameEvent _onPickUp;
    [SerializeField] PlayerData _player;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_player.CoffeAmount < 3)
        {
            _onPickUp.Invoke();
            _player.CoffeAmount++;
            gameObject.SetActive(false);
        }
    }
}
